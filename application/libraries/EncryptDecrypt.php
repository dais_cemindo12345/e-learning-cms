<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EncryptDecrypt
{
    public function dais_encrypt($text)
    {
        $key = "dais12345";
        //$key previously generated safely, ie: openssl_random_pseudo_bytes
        $plaintext = $text;
        $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options = OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary = true);
        $ciphertext = base64_encode($iv . $hmac . $ciphertext_raw);

        return $ciphertext;
    }

    public function dais_decrypt($chipertext)
    {

        $c = base64_decode(str_replace(' ', '+', $chipertext));
        $key = "dais12345";
        $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len = 32);
        $ciphertext_raw = substr($c, $ivlen + $sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options = OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary = true);

        return $original_plaintext;
    }
}
