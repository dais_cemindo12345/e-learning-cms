<?php

class Kelas_model extends CI_Model
{
    public function getClassJoinMasterStatus($apiPath)
    {
        $urlGetClass = $this->getKey->url() . $apiPath;

        $paramsGetClass = [
            'X-API-KEY' => $this->getKey->api_key()
        ];

        $apiResult = $this->api->post($urlGetClass, $paramsGetClass);

        return $apiResult;
    }

    public function getClassJoinMasterStatusById($apiPath, $id)
    {
        $urlGetClass = $this->getKey->url() . $apiPath;

        $paramsGetClass = [
            'X-API-KEY' => $this->getKey->api_key(),
            'id'        => $id
        ];

        $apiResult = $this->api->post($urlGetClass, $paramsGetClass);

        return $apiResult;
    }

    public function getDataById($apiPath, $id)
    {
        $urlGetClass = $this->getKey->url() . $apiPath;

        $paramsGetClass = [
            'X-API-KEY' => $this->getKey->api_key(),
            'id'        => $id
        ];

        $apiResult = $this->api->post($urlGetClass, $paramsGetClass);

        return $apiResult;
    }

    public function getData($apiPath)
    {
        $urlGetClass = $this->getKey->url() . $apiPath;

        $paramsGetClass = [
            'X-API-KEY' => $this->getKey->api_key()
        ];

        $apiResult = $this->api->post($urlGetClass, $paramsGetClass);

        return $apiResult;
    }

    public function getDataParams($apiPath, $params)
    {
        $urlGetClass = $this->getKey->url() . $apiPath;

        $paramsGetClass = [
            'X-API-KEY' => $this->getKey->api_key(),
            'params' => $params
        ];

        $apiResult = $this->api->post($urlGetClass, $paramsGetClass);

        return $apiResult;
    }
}
