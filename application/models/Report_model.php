<?php

class Report_model extends CI_Model
{
    public function getDataParams($apiPath, $params)
    {
        $urlGetClass = $this->getKey->url() . $apiPath;

        $paramsGetClass = [
            'X-API-KEY' => $this->getKey->api_key(),
            'params' => $params
        ];

        $apiResult = $this->api->post($urlGetClass, $paramsGetClass);

        return $apiResult;
    }

    public function getAll($apiPath)
    {
        $urlGetClass = $this->getKey->url() . $apiPath;

        $paramsGetClass = [
            'X-API-KEY' => $this->getKey->api_key()
        ];

        $apiResult = $this->api->post($urlGetClass, $paramsGetClass);

        return $apiResult;
    }
}
