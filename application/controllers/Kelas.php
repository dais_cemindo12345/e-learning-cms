<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // jika tidak ada session
        if (!$this->session->userdata('nik')) {
            redirect('auth');
        }

        $this->load->library("form_validation");
        $this->load->library("encryptdecrypt");
        $this->load->model('API', 'api');
        $this->load->model('Getkey_model', 'getKey');
        $this->load->model('Kelas_model', 'kelas');

        // timezone
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {
        //get data class join master status 
        $data['getClass'] = $this->kelas->getClassJoinMasterStatus('Elearning/eLearning_get_class/');

        // get kode kopetensi
        $data['getCopetence'] = $this->kelas->getData('Elearning/eLearning_get_codeCopetence/');

        // get kode unit bisnis
        $data['getBusinessUnit'] = $this->kelas->getData('Elearning/eLearning_get_businessUnit/');

        // title
        $data['title'] = "Kelas";
        // more class (must be in every content)
        $class['moreClass'] = "d-flex";

        // loade view
        $this->load->view('templates/header', $data);
        $this->load->view('templates/topbar');
        $this->load->view('templates/wrapper', $class);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/content');
        $this->load->view('content/kelas/index', $data);
        $this->load->view('templates/end-content');
        $this->load->view('templates/end-wrapper');
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
        $this->form_validation->set_rules(
            'modul_id',
            'Modul id',
            'trim|required',
            ['required' => "%s tidak boleh kosong."]
        );
        $this->form_validation->set_rules(
            'kode_kelas',
            'Kode kelas',
            'trim|required',
            ['required' => "%s tidak boleh kosong."]
        );
        $this->form_validation->set_rules(
            'kode_sub_kelas',
            'Kode sub kelas',
            'trim|required',
            ['required' => "%s tidak boleh kosong."]
        );
        $this->form_validation->set_rules(
            'judul',
            'Judul',
            'trim|required',
            ['required' => "%s tidak boleh kosong."]
        );
        $this->form_validation->set_rules(
            'tujuan_pelatihan',
            'Tujuan Pelatihan',
            'trim|required',
            ['required' => "%s tidak boleh kosong."]
        );
        $this->form_validation->set_rules(
            'tgl_aktif',
            'Tanggal aktif kelas',
            'trim|required',
            ['required' => "%s tidak boleh kosong."]
        );
        $this->form_validation->set_rules(
            'tgl_kadaluarsa',
            'Tanggal kadaluarsa kelas',
            'trim|required',
            ['required' => "%s tidak boleh kosong."]
        );

        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            // create final code
            $kodeKopetensi = $this->input->post('kode_kopetensi');
            $kodeUnitBisnis = $this->input->post('kode_unit_bisnis');
            $modulId = $this->input->post('modul_id');
            $kodeKelas = $this->input->post('kode_kelas');
            $kodeSubKelas = $this->input->post('kode_sub_kelas');
            $finalCode = $kodeKopetensi . $kodeUnitBisnis . "-" . $modulId . "." . $kodeKelas . "-" . $kodeSubKelas;

            // Check final code
            $finalCodeCheck = $this->kelas->getDataById('Elearning/eLearning_check_finalcode/', $finalCode);

            if ($finalCodeCheck == true) {
                if (!empty($_FILES['gambar_kelas']['name'])) {
                    // do upload
                    $picture = $this->do_upload();

                    // moduleParams
                    $namaModul = $this->input->post('nama_modul');

                    // $moduleParams = [
                    //     'moduleId'     => htmlspecialchars($modulId),
                    //     'moduleStatus' => 1
                    // ];

                    // subClassParams
                    $judulSubKelas = $this->input->post('judul_sub_kelas');

                    // $subClassParams = [
                    //     'subClassId'     => htmlspecialchars($kodeSubKelas),
                    //     'subClassStatus' => 1
                    // ];

                    // classParams                
                    $classParams = [
                        'title'              => htmlspecialchars($this->input->post("judul")),
                        'pictureName'        => htmlspecialchars($picture['filename']),
                        'trainingObjectives' => htmlspecialchars($this->input->post("tujuan_pelatihan")),
                        'statusId'           => htmlspecialchars(1),
                        'imageStatus'        => htmlspecialchars(1),
                        'dateCreated'        => htmlspecialchars(date('Y-m-d H:i:s')),
                        'dateModified'       => htmlspecialchars(''),
                        'modifiedBy'         => htmlspecialchars($this->session->userdata('nik')),
                        'activeClass'        => htmlspecialchars($this->input->post("tgl_aktif")),
                        'expiredClass'       => htmlspecialchars($this->input->post("tgl_kadaluarsa")),
                        'initialOfCopetence' => htmlspecialchars($kodeKopetensi),
                        'businessUnitId'     => htmlspecialchars($kodeUnitBisnis),
                        'moduleId'           => htmlspecialchars($modulId),
                        'moduleName'         => htmlspecialchars($namaModul),
                        'classCode'          => htmlspecialchars($kodeKelas),
                        'subClassId'         => htmlspecialchars($kodeSubKelas),
                        'subClassName'       => htmlspecialchars($judulSubKelas),
                        'finalCode'          => htmlspecialchars($finalCode),
                    ];

                    $url = $this->getKey->url() . "elearning/eLearning_insert_class/";
                    $params = [
                        'X-API-KEY'          => htmlspecialchars($this->getKey->api_key()),
                        // 'module'             => $moduleParams,
                        // 'subClass'           => $subClassParams,
                        'class'              => $classParams
                    ];

                    $apiResult = $this->api->post($url, $params);
                    if ($apiResult == true) {

                        // move upload
                        $remotePath = MOVE_UPLOADED_CLASS_IMAGE;
                        $moveUpload = $this->move_to_public($picture['filename'], $picture['filepath'], $remotePath);

                        if ($moveUpload['status'] == FALSE) {
                            // get data file name dan file path
                            $fn = $picture['filename'];
                            $fp = $picture['filepath'];
                            // encryp
                            $efn = $this->encryptdecrypt->dais_encrypt($fn);
                            $efp = $this->encryptdecrypt->dais_encrypt($fp);

                            $this->session->set_flashdata('message', '
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    ' . $moveUpload['message'] . ' <br />
                                    <a class="btn btn-primary" href="' . base_url() . 'kelas/reupload?fn=' . $efn . '&fp=' . $efp . '">Unggah ulang</a>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            ');

                            redirect('kelas');
                        } else {
                            $this->session->set_flashdata('message', '
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    ' . $apiResult['message'] . '
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            ');

                            redirect('kelas');
                        }
                    } else {
                        $name = $picture['filename'];
                        $data_name = explode('.', $name);
                        $new_name_image =  $data_name[0] . "_com." . $data_name[1];
                        @unlink($new_name_image);

                        $this->session->set_flashdata('message', '
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ' . $apiResult['message'] . '
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        ');

                        redirect('kelas');
                    }
                } else {
                    $this->session->set_flashdata('message', '
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    Gambar kelas tidak boleh kosong.
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                  </div>
                    ');

                    redirect('kelas');
                }
            } else {
                $this->session->set_flashdata('message', '
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                Kode final sudah ada
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
                ');

                redirect('kelas');
            }
        }
    }

    public function check_final_code()
    {
        $id = $this->input->post('finalCode');

        $url = 'Elearning/eLearning_check_finalcode/';

        $dataClass = $this->kelas->getDataById($url, $id);

        $resultDataClass = $dataClass['status'];

        $data = [
            'return'     => $resultDataClass,
        ];
        echo json_encode($data);
    }

    public function get_class_by_id_for_js()
    {
        $id = $this->encryptdecrypt->dais_decrypt($this->input->post('id'));

        $url = 'Elearning/eLearning_get_class_by_id/';

        $dataClass = $this->kelas->getClassJoinMasterStatusById($url, $id);

        $resultDataClass = $dataClass['result'][0];

        $data = [
            'idEncrypt'     => $this->input->post('id'),
            'dataClass'     => $resultDataClass
        ];
        echo json_encode($data);
    }

    public function update_class()
    {
        $this->form_validation->set_rules(
            'ubah_judul',
            'Judul',
            'trim|required',
            ['required' => "%s tidak boleh kosong."]
        );
        $this->form_validation->set_rules(
            'ubah_tujuan_pelatihan',
            'Tujuan Pelatihan',
            'trim|required',
            ['required' => "%s tidak boleh kosong."]
        );
        $this->form_validation->set_rules(
            'ubah_tgl_aktif',
            'Tanggal aktif',
            'trim|required',
            ['required' => "%s tidak boleh kosong."]
        );
        $this->form_validation->set_rules(
            'ubah_tgl_kadaluarsa',
            'Tanggal kadaluarsa',
            'trim|required',
            ['required' => "%s tidak boleh kosong."]
        );

        if ($this->form_validation->run() == FALSE) {

            $this->index();
        } else {

            // check new image
            $new_image = $_FILES['ubah_gambar_kelas']['name'];

            if (!$new_image) {

                $image = $this->input->post('old_image');

                // update data class
                $url = $this->getKey->url() . 'elearning/eLearning_update_data_class/';
                $params = [
                    'X-API-KEY'             => $this->getKey->api_key(),
                    'title'                 => $this->input->post('ubah_judul'),
                    'activeClass'           => $this->input->post('ubah_tgl_aktif'),
                    'expiredClass'          => $this->input->post('ubah_tgl_kadaluarsa'),
                    'trainingObjectives'    => $this->input->post('ubah_tujuan_pelatihan'),
                    'pictureName'           => $image,
                    'dateModified'          => date('Y-m-d H:i:s'),
                    'modifiedBy'            => $this->session->userdata('nik'),
                    'id'                    => $this->encryptdecrypt->dais_decrypt($this->input->post('id_class'))
                ];

                $update_data_class = $this->api->post($url, $params);

                if ($update_data_class['status'] == FALSE) {
                    $this->session->set_flashdata('message', '
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    ' . $update_data_class['message'] . '
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                ');

                    redirect('kelas');
                } else {
                    $this->session->set_flashdata('message', '
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                            ' . $update_data_class['message'] . '
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        ');

                    redirect('kelas');
                }
            } else {

                $picture = $this->do_upload_update_class();
                print_r($picture);
                die;

                // move upload
                $remotePath = MOVE_UPLOADED_CLASS_IMAGE;
                $moveUpload = $this->move_to_public_js($picture['filename'], $picture['filepath'], $remotePath);

                if ($moveUpload == FALSE) {

                    $name = $picture['filename'];
                    $data_name = explode('.', $name);
                    $new_name_image =  $data_name[0] . "_com." . $data_name[1];

                    $source = $picture['filepath'] . $new_name_image;
                    @unlink($source);

                    $this->session->set_flashdata('message', '
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        Kesalahan pada jaringan, silahkan coba beberapa saat lagi.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    ');

                    redirect($_SERVER['HTTP_REFERER']);
                } else {

                    $name = $picture['filename'];
                    $data_name = explode('.', $name);
                    $new_name_image =  $data_name[0] . "_com." . $data_name[1];

                    $source = $picture['filepath'] . $new_name_image;
                    @unlink($source);

                    $image = $picture['filename'];

                    // update data class
                    $url = $this->getKey->url() . 'elearning/eLearning_update_data_class/';
                    $params = [
                        'X-API-KEY'             => $this->getKey->api_key(),
                        'title'                 => $this->input->post('ubah_judul'),
                        'activeClass'           => $this->input->post('ubah_tgl_aktif'),
                        'expiredClass'          => $this->input->post('ubah_tgl_kadaluarsa'),
                        'trainingObjectives'    => $this->input->post('ubah_tujuan_pelatihan'),
                        'pictureName'           => $image,
                        'dateModified'          => date('Y-m-d H:i:s'),
                        'modifiedBy'            => '03302760',
                        'id'                    => $this->encryptdecrypt->dais_decrypt($this->input->post('id_class'))
                    ];

                    $update_data_class = $this->api->post($url, $params);

                    if ($update_data_class['status'] == FALSE) {
                        $name = $picture['filename'];
                        $data_name = explode('.', $name);
                        $new_name_image =  $data_name[0] . "_com." . $data_name[1];
                        @unlink($new_name_image);

                        $connection = ssh2_connect('report-id.online', 22);
                        ssh2_auth_password($connection, 'dais', 'Dais_cg#2020');
                        $sftp = ssh2_sftp($connection);

                        ssh2_sftp_unlink($sftp, MOVE_UPLOADED_CLASS_IMAGE . $image);

                        $this->session->set_flashdata('message', '
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ' . $update_data_class['message'] . '
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        ');

                        redirect('kelas');
                    } else {

                        $connection = ssh2_connect('report-id.online', 22);
                        ssh2_auth_password($connection, 'dais', 'Dais_cg#2020');
                        $sftp = ssh2_sftp($connection);

                        ssh2_sftp_unlink($sftp, MOVE_UPLOADED_CLASS_IMAGE . $this->input->post('old_image'));

                        $this->session->set_flashdata('message', '
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                            ' . $update_data_class['message'] . '
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        ');

                        redirect('kelas');
                    }
                }
            }
        }
    }

    public function do_upload()
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'jpeg|jpg|png|tiff';
        $config['max_size']             = 2048;

        // encrypName
        $config['encrypt_name']             = TRUE;
        $new_name = time() . $_FILES["gambar_kelas"]['name'];
        $config['file_name'] = $new_name;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('gambar_kelas')) {
            //Get uploaded file information
            $upload_data = $this->upload->data();
            $filename = $upload_data['file_name'];
            $filepath = $upload_data['file_path'];
            $fullPath = $upload_data['full_path'];

            // $this->compress_image->compress();
            $img_config['image_library'] = 'gd2';
            $img_config['source_image'] = $fullPath;
            $img_config['create_thumb'] = TRUE;
            $img_config['maintain_ratio'] = TRUE;
            $img_config['quality'] = '50%';

            // cek width
            if ($upload_data['image_width'] > 1024) {
                $img_config['width']         = 1024;
            } else {
                $img_config['width']         = $upload_data['image_width'];
            }
            // cek height
            if ($upload_data['image_width'] > 768) {
                $img_config['height']         = 768;
            } else {
                $img_config['height']       = $upload_data['image_height'];
            }

            $img_config['thumb_marker'] = '_com';

            $this->load->library('image_lib', $img_config);

            $this->image_lib->resize();

            //Delete file from local server
            @unlink($fullPath);

            $data = [
                'filename' => $filename,
                'filepath' => $filepath
            ];

            return $data;
        } else {
            $error = array('error' => $this->upload->display_errors());

            $this->session->set_flashdata('message', '
             <div class="alert alert-danger alert-dismissible fade show" role="alert">
             ' . $error['error'] . '
             <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
           </div>
             ');

            redirect($_SERVER['HTTP_REFERER']);

            exit;
        }
    }

    public function do_upload_update_class()
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'jpeg|jpg|png|tiff';
        $config['max_size']             = 2048;

        // encrypName
        $config['encrypt_name']             = TRUE;
        $new_name = time() . $_FILES["ubah_gambar_kelas"]['name'];
        $config['file_name'] = $new_name;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('ubah_gambar_kelas')) {
            //Get uploaded file information
            $upload_data = $this->upload->data();
            $filename = $upload_data['file_name'];
            $filepath = $upload_data['file_path'];
            $fullPath = $upload_data['full_path'];

            // $this->compress_image->compress();
            $img_config['image_library'] = 'gd2';
            $img_config['source_image'] = $fullPath;
            $img_config['create_thumb'] = TRUE;
            $img_config['maintain_ratio'] = TRUE;
            $img_config['quality'] = '50%';

            // cek width
            if ($upload_data['image_width'] > 1024) {
                $img_config['width']         = 1024;
            } else {
                $img_config['width']         = $upload_data['image_width'];
            }
            // cek height
            if ($upload_data['image_width'] > 768) {
                $img_config['height']         = 768;
            } else {
                $img_config['height']       = $upload_data['image_height'];
            }

            $img_config['thumb_marker'] = '_com';

            $this->load->library('image_lib', $img_config);

            $this->image_lib->resize();

            //Delete file from local server
            @unlink($fullPath);

            $data = [
                'filename' => $filename,
                'filepath' => $filepath
            ];

            return $data;
        } else {
            $error = array('error' => $this->upload->display_errors());

            $this->session->set_flashdata('message', '
             <div class="alert alert-danger alert-dismissible fade show" role="alert">
             ' . $error['error'] . '
             <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
           </div>
             ');

            redirect($_SERVER['HTTP_REFERER']);

            exit;
        }
    }

    public function move_to_public($fileName, $filePath, $remotePath)
    {
        $name = $fileName;
        $data_name = explode('.', $name);
        $new_name_image =  $data_name[0] . "_com." . $data_name[1];

        $file_local = $filePath . $new_name_image;
        $file_remote = $remotePath . $fileName;
        $server = 'report-id.online';
        $serverPort = '22';
        $serverUser = 'dais';
        $serverPassword = 'Dais_cg#2020';
        $connection = ssh2_connect($server, $serverPort);

        $login = ssh2_auth_password($connection, $serverUser, $serverPassword);

        $send = ssh2_scp_send($connection, $file_local, $file_remote, 0644);

        if ($send == true) {
            $source = $filePath . $new_name_image;
            @unlink($source);

            $data = [
                'status' => TRUE,
                'message' => 'Berhasil di upload'
            ];
        } else {
            // ubah status gambar
            $url = $this->getKey->url() . "elearning/eLearning_update_class/";
            $params = [
                'X-API-KEY' => $this->getKey->api_key(),
                'pictureName' => $fileName,
                'imageStatus' => 0
            ];

            $this->api->post($url, $params);

            $data = [
                'status' => FALSE,
                'message' => 'Kesalahan jaringan pada saat upload, silahkan coba kembali'
            ];
        }

        ssh2_exec($connection, 'exit');

        return $data;
    }

    public function move_to_public_js($fileName, $filePath, $remotePath)
    {
        $name = $fileName;
        $data_name = explode('.', $name);
        $new_name_image =  $data_name[0] . "_com." . $data_name[1];

        $file_local = $filePath . $new_name_image;
        $file_remote = $remotePath . $fileName;
        $server = 'report-id.online';
        $serverPort = '22';
        $serverUser = 'dais';
        $serverPassword = 'Dais_cg#2020';
        $connection = ssh2_connect($server, $serverPort);

        $login = ssh2_auth_password($connection, $serverUser, $serverPassword);

        $send = ssh2_scp_send($connection, $file_local, $file_remote, 0644);



        ssh2_exec($connection, 'exit');

        return $send;
    }

    public function reupload()
    {
        $fileName = $this->encryptdecrypt->dais_decrypt($this->input->get('fn'));
        $filePath = $this->encryptdecrypt->dais_decrypt($this->input->get('fp'));

        $moveUpload = $this->move_to_public($fileName, $filePath, MOVE_UPLOADED_CLASS_IMAGE);

        if ($moveUpload['status'] == FALSE) {
            // get data file name dan file path
            $fn = $fileName;
            $fp = $filePath;
            // encryp
            $efn = $this->encryptdecrypt->dais_encrypt($fn);
            $efp = $this->encryptdecrypt->dais_encrypt($fp);

            $this->session->set_flashdata('message', '
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    ' . $moveUpload['message'] . ' <br />
                    <a class="btn btn-primary" href="' . base_url() . 'kelas/reupload?fn=' . $efn . '&fp=' . $efp . '">Unggah ulang</a>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            ');

            redirect('kelas');
        } else {
            // ubah status gambar
            $url = $this->getKey->url() . "elearning/eLearning_update_class/";
            $params = [
                'X-API-KEY' => $this->getKey->api_key(),
                'pictureName' => $fileName,
                'imageStatus' => 1
            ];

            $this->api->post($url, $params);

            $this->session->set_flashdata('message', '
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                    ' . $moveUpload['message'] . '
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            ');

            redirect('kelas');
        }
    }

    public function lampiran()
    {
        # get id class from url
        $idClass = $this->input->get('c');
        $id = $this->encryptdecrypt->dais_decrypt($idClass);

        # get clas join master status by id
        $dataClass = $this->kelas->getClassJoinMasterStatusById('Elearning/eLearning_get_class_by_id/', $id);
        $data['dataClass'] = $dataClass['result'][0];

        # Get attachment by Id
        $dataAttachmentClass = $this->kelas->getClassJoinMasterStatusById('Elearning/eLearning_get_attachment_by_id/', $id);
        $data['dataAttachmentClass'] = $dataAttachmentClass;

        # title
        $data['title'] = "Kelas-Lampiran";

        # more class (must be in every content)
        $class['moreClass'] = "d-flex";

        // load view
        $this->load->view('templates/header', $data);
        $this->load->view('templates/topbar');
        $this->load->view('templates/wrapper', $class);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/content');
        $this->load->view('content/kelas/lampiran', $data);
        $this->load->view('templates/end-content');
        $this->load->view('templates/end-wrapper');
        $this->load->view('templates/footer');
    }

    public function add_attachment()
    {
        # Set validation
        $this->form_validation->set_rules(
            'judul',
            'Judul',
            'trim|required',
            ['required' => "%s tidak boleh kosong."]
        );

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                ' . validation_errors() . '
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
                ');

            redirect($_SERVER['HTTP_REFERER']);
        } else {
            if (!empty($_FILES['attachment']['name'])) {
                $upload = $this->do_upload_attachment($this->input->post('lampiran'));

                $remotePath = MOVE_UPLOADED_CLASS_ATTACHMENT;

                $moveUpload = $this->move_uploade_attachment($upload['filename'], $upload['filepath'], $remotePath);

                if ($moveUpload['status'] == FALSE) {

                    $this->session->set_flashdata('message', '
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        ' . $moveUpload['message'] . '
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    ');

                    redirect($_SERVER['HTTP_REFERER']);
                } else {

                    $classId = $this->encryptdecrypt->dais_decrypt($this->input->post('classId'));

                    $params = [
                        'X-API-KEY' => $this->getKey->api_key(),
                        'classId' => $classId,
                        'title' => $this->input->post('judul'),
                        'attachmentType' => $this->input->post('lampiran'),
                        'attachmentName' => $upload['filename']
                    ];

                    $url = $this->getKey->url() . 'elearning/eLearning_insert_attachment/';

                    $result = $this->api->post($url, $params);

                    if ($result['status'] == FALSE) {
                        $this->session->set_flashdata('message', '
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                ' . $result['message'] . '
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        ');

                        // delet file on server public
                        $connection = ssh2_connect('report-id.online', 22);
                        ssh2_auth_password($connection, 'dais', 'Dais_cg#2020');
                        $sftp = ssh2_sftp($connection);

                        ssh2_sftp_unlink($sftp, MOVE_UPLOADED_CLASS_ATTACHMENT . $this->input->post('judul'));

                        redirect($_SERVER['HTTP_REFERER']);
                    } else {
                        $this->session->set_flashdata('message', '
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                ' . $result['message'] . '
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        ');

                        redirect($_SERVER['HTTP_REFERER']);
                    }
                }
            } else {
                $this->session->set_flashdata('message', '
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                File lampiran tidak boleh kosong.
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
                ');

                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

    public function do_upload_attachment($type)
    {
        $config['upload_path']          = './uploads/';
        if ($type == "dokumen") {
            # dokumen
            $config['allowed_types']        = 'pdf';
        } elseif ($type == "gambar") {
            # gambar
            $config['allowed_types']        = 'jpeg|jpg|png';
        } else {
            # video
            $config['allowed_types']        = 'mp4|mp3';
        }

        // encrypName
        $config['encrypt_name']             = TRUE;
        $new_name = time() . $_FILES["attachment"]['name'];
        $config['file_name'] = $new_name;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('attachment')) {
            //Get uploaded file information
            $upload_data = $this->upload->data();
            $filename = $upload_data['file_name'];
            $filepath = $upload_data['file_path'];
            $fullpath = $upload_data['full_path'];

            $data = [
                'filename' => $filename,
                'filepath' => $filepath,
                'fullpath' => $fullpath
            ];

            return $data;
        } else {
            $error = array('error' => $this->upload->display_errors());

            $this->session->set_flashdata('message', '
             <div class="alert alert-danger alert-dismissible fade show" role="alert">
             ' . $error['error'] . '
             <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
           </div>
             ');

            redirect($_SERVER['HTTP_REFERER']);

            exit;
        }
    }

    public function move_uploade_attachment($fileName, $filePath, $remotePath)
    {

        # Server config
        $file_local = $filePath . $fileName;
        $file_remote = $remotePath . $fileName;
        $server = 'report-id.online';
        $serverPort = '22';
        $serverUser = 'dais';
        $serverPassword = 'Dais_cg#2020';
        $connection = ssh2_connect($server, $serverPort);

        $login = ssh2_auth_password($connection, $serverUser, $serverPassword);

        $send = ssh2_scp_send($connection, $file_local, $file_remote, 0644);
        if ($send == true) {

            $source = $file_local;
            @unlink($source);

            $data = [
                'status' => TRUE,
                'message' => 'Berhasil di upload'
            ];

            return $data;
        } else {

            $source = $file_local;
            @unlink($source);

            $data = [
                'status' => FALSE,
                'message' => 'Gagal di upload'
            ];

            return $data;
        }
    }

    public function deleteAttachment()
    {
        $id = $this->encryptdecrypt->dais_decrypt($this->input->get('d'));
        $file = $this->encryptdecrypt->dais_decrypt($this->input->get('f'));

        $url = $this->getKey->url() . 'Elearning/eLearning_delete_attachment/';

        $params = [
            'X-API-KEY' => $this->getKey->api_key(),
            'id'        => $id
        ];

        $result = $this->api->post($url, $params);

        if ($result['status'] == TRUE) {

            $this->session->set_flashdata('message', '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                ' . $result['message'] . '
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            ');

            // delet file on server local
            $connection = ssh2_connect('report-id.online', 22);
            ssh2_auth_password($connection, 'dais', 'Dais_cg#2020');
            $sftp = ssh2_sftp($connection);

            ssh2_sftp_unlink($sftp, MOVE_UPLOADED_CLASS_ATTACHMENT . $file);

            redirect($_SERVER['HTTP_REFERER']);
        } else {

            $this->session->set_flashdata('message', '
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                ' . $result['message'] . '
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            ');

            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function delete_class()
    {

        # Get id 
        $id = $this->encryptdecrypt->dais_decrypt($this->input->get('d'));

        $urlClassDelete = $this->getKey->url() . 'Elearning/eLearning_delete_class/';

        $paramsClassDelete = [
            'X-API-KEY' => $this->getKey->api_key(),
            'id'        => $id
        ];

        $classDelete = $this->api->post($urlClassDelete, $paramsClassDelete);

        if ($classDelete['status'] === TRUE) {
            $data = $classDelete['data'];

            $connection = ssh2_connect('report-id.online', 22);
            ssh2_auth_password($connection, 'dais', 'Dais_cg#2020');
            $sftp = ssh2_sftp($connection);

            ssh2_sftp_unlink($sftp, MOVE_UPLOADED_CLASS_IMAGE . $classDelete['pictureName']);

            foreach ($data as $d) {
                ### delete attachment from public server
                ssh2_sftp_unlink($sftp, MOVE_UPLOADED_CLASS_ATTACHMENT . $d['attachmentName']);
            }

            $this->session->set_flashdata('message', '
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    ' . $classDelete['message'] . '
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    ');

            redirect('kelas');
        } else {
            $this->session->set_flashdata('message', '
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    ' . $classDelete['message'] . '
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            ');

            redirect('kelas');
        }
    }

    public function soal()
    {
        # get id class from url
        $idClass = $this->input->get('c');
        $id = $this->encryptdecrypt->dais_decrypt($idClass);

        # get clas join master status by id
        $dataClass = $this->kelas->getClassJoinMasterStatusById('Elearning/eLearning_get_class_by_id/', $id);
        $data['dataClass'] = $dataClass['result'][0];

        // Get question by id Class
        $dataShowQuestion = $this->kelas->getDataById('Elearning/eLearning_get_question_by_id_class/', $id);
        $data['dataShowQuestion'] = $dataShowQuestion['status'];
        $data['showQuestion'] = $dataShowQuestion['result'];

        $dataShowOption = $this->kelas->getDataById('Elearning/eLearning_get_option_question_by_question_code/', $id);

        # title
        $data['title'] = "Kelas-Soal";

        # more class (must be in every content)
        $class['moreClass'] = "d-flex";

        // load view
        $this->load->view('templates/header', $data);
        $this->load->view('templates/topbar');
        $this->load->view('templates/wrapper', $class);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/content');
        $this->load->view('content/kelas/soal', $data);
        $this->load->view('templates/end-content');
        $this->load->view('templates/end-wrapper');
        $this->load->view('templates/footer');
    }

    public function add_soal()
    {

        $str_json = file_get_contents('php://input');

        $data = json_decode($str_json);

        $datType = $data->questionType; // Type question [mc => multiple choice, p]

        $idClass = $this->encryptdecrypt->dais_decrypt($data->id);
        $questionCode = uniqid(); // Create unique Question code
        $question = $data->question;
        $questionType = $data->questionType;
        $point = $data->point;


        if ($datType == "mc") {
            $answerKey = $data->answerKey;
            $answer = $data->answer;

            // Question
            $dataQuestion = [
                "idClass"       => htmlspecialchars($idClass),
                "questionCode"  => htmlspecialchars($questionCode),
                "question"      => htmlspecialchars($question),
                "questionType"  => htmlspecialchars($questionType),
                "questionPoint"         => htmlspecialchars($point),
            ];

            // Answer
            $arr = [];

            for ($i = 0; $i < count($answer); $i++) {
                for ($j = $i; $j <= $i; $j++) {
                    $data = [
                        "questionCode"  => htmlspecialchars($questionCode),
                        "option"        => htmlspecialchars($answer[$i]),
                        "correctAnswer" => $answerKey[$j],
                    ];
                }

                array_push($arr, $data);
            }

            $dataAnswer = $arr;

            $urlQuestionMc = $this->getKey->url() . 'Elearning/eLearning_insert_question_mc';
            $paramQuestionMc = [
                "X-API-KEY"     => $this->getKey->api_key(),
                "dataQuestion"      => $dataQuestion,
                "dataAnswer"        => $dataAnswer
            ];

            $resultQuestionMc = $this->api->post($urlQuestionMc, $paramQuestionMc);

            echo json_encode($resultQuestionMc);
        } elseif ($datType == "p") {

            $dataQuestion = [
                "idClass"       => htmlspecialchars($idClass),
                "questionCode"  => htmlspecialchars($questionCode),
                "question"      => htmlspecialchars($question),
                "questionPoint" => htmlspecialchars($point),
                "questionType"  => htmlspecialchars($questionType)
            ];

            $urlQuestionP = $this->getKey->url() . 'Elearning/eLearning_insert_question_p';
            $paramQuestionP = [
                "X-API-KEY"     => $this->getKey->api_key(),
                "dataQuestion"      => $dataQuestion
            ];

            $resultQuestionP = $this->api->post($urlQuestionP, $paramQuestionP);

            echo json_encode($resultQuestionP);
        }
    }

    public function penugasan()
    {
        if (isset($_POST['search'])) {
            $search = htmlspecialchars($this->input->post('search_data_karyawan'));

            $result = $this->kelas->getDataById('Elearning/hcis_get_data_karyawan/', $search);


            $data['dataKaryawanStatus'] = $result['status'];
            $data['dataKaryawan'] = $result['result'];
        }

        # get clas join master status by id
        $classTitle = $this->input->get('t');
        $data['classTitle'] = $classTitle;

        # Get data assignment by class
        $getC = $this->input->get('c');
        $classId = $this->encryptdecrypt->dais_decrypt($getC);
        $dataAssignmentByClassId = $this->kelas->getDataById('Elearning/eLearning_get_assignment_by_classid/', $classId);
        $data['assignmentByClassId'] = $dataAssignmentByClassId;

        # title
        $data['title'] = "Kelas-Penugasan";

        # more class (must be in every content)
        $class['moreClass'] = "d-flex";

        // load view
        $this->load->view('templates/header', $data);
        $this->load->view('templates/topbar');
        $this->load->view('templates/wrapper', $class);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/content');
        $this->load->view('content/kelas/penugasan', $data);
        $this->load->view('templates/end-content');
        $this->load->view('templates/end-wrapper');
        $this->load->view('templates/footer');
    }

    public function assign_employees()
    {
        $str_json = file_get_contents('php://input');

        $data = json_decode($str_json);

        $idClass = $this->encryptdecrypt->dais_decrypt($data->kelas);

        // get data class
        $url = 'Elearning/eLearning_get_class_by_id_row/';
        $dataClass = $this->kelas->getDataById($url, $idClass);
        $finalCode = $dataClass['result']['finalCode'];

        // Class Code
        $initialOfCopetence = $dataClass['result']['initialOfCopetence'];
        $businessUnitId = $dataClass['result']['businessUnitId'];
        $moduleId = $dataClass['result']['moduleId'];
        $classCode = $dataClass['result']['classCode'];

        $createClassCode = $initialOfCopetence . $businessUnitId . '-' . $moduleId . '.' . $classCode;


        $url = $this->getKey->url() . "Elearning/eLearning_insert_assignment/";

        $params = [
            'X-API-KEY'        => $this->getKey->api_key(),
            'employeeId'       => $data->id,
            'employeeNIK'      => $data->nik,
            'employeeName'     => $data->nama,
            'classId'          => $idClass,
            'classCode'        => $createClassCode,
            'finalCode'        => $finalCode,
            'assignmentStatus' => 4
        ];

        $result = $this->api->post($url, $params);

        echo json_encode($result);
    }

    public function delete_assignment()
    {
        $str_json = file_get_contents('php://input');

        $data = json_decode($str_json);

        $idClass = $this->encryptdecrypt->dais_decrypt($data->kelas);

        $url = $this->getKey->url() . "Elearning/eLearning_delete_assignment/";

        $params = [
            'X-API-KEY'     => $this->getKey->api_key(),
            'employeeNIK'   => strval($data->nik),
            'classId'       => intval($idClass)
        ];

        $result = $this->api->post($url, $params);

        echo json_encode($result);
    }

    public function publish()
    {
        $classId = $this->encryptdecrypt->dais_decrypt($this->input->get('c'));

        // Check attachment
        $attachmentCheck = $this->kelas->getDataById('Elearning/eLearning_get_attachment_by_id/', $classId);

        // var_dump($attachmentCheck);
        // die;

        if ($attachmentCheck['status'] == false) {

            $this->session->set_flashdata('message', '
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    Lampiran Kelas tidak boleh kosong!
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            ');

            redirect('kelas');

            exit();
        }

        // Check soal
        $questionCheck = $this->kelas->getDataById('Elearning/eLearning_get_question_by_id_class/', $classId);

        if ($questionCheck['status'] == false) {

            $this->session->set_flashdata('message', '
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    Soal Kelas tidak boleh kosong!
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            ');

            redirect('kelas');

            exit();
        }

        // Check penugasan
        $assignmentCheck = $this->kelas->getDataById('Elearning/eLearning_get_assignment_by_classid/', $classId);

        if ($assignmentCheck['status'] == false) {

            $this->session->set_flashdata('message', '
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    Belum ada Karyawan yang di tugaskan!
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            ');

            redirect('kelas');

            exit();
        }

        $url = $this->getKey->url() . 'Elearning/eLearning_update_data_class_publish/';

        $params = [
            "X-API-KEY" => $this->getKey->api_key(),
            "id"        => $classId,
            "statusId"  => 2
        ];

        $result = $this->api->post($url, $params);

        if ($result['status'] == false) {
            $this->session->set_flashdata('message', '
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    Kelas gagal di <strong>Publish!</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            ');

            redirect('kelas');
        }

        if ($result['status'] == true) {
            $this->session->set_flashdata('message', '
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    Kelas berhasil di <strong>Publish!</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            ');

            redirect('kelas');
        }
    }

    public function delete_question()
    {
        $getId = $this->input->get("qc");
        $classId = $this->input->get("c");
        $questionCode = $this->encryptdecrypt->dais_decrypt($getId);

        $url = $this->getKey->url() . 'Elearning/eLearning_delete_question/';
        $data = [
            "X-API-KEY"    => $this->getKey->api_key(),
            "questionCode" => $questionCode
        ];

        $result = $this->api->post($url, $data);

        var_dump($result);

        if ($result['status'] == false) {
            $this->session->set_flashdata('message', '
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                ' . $result['message'] . '
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            ');
        } else {
            $this->session->set_flashdata('message', '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                ' . $result['message'] . '
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            ');
        }

        redirect('kelas/soal?c=' . $classId);
    }
}
