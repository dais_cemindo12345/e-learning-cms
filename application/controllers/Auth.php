<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->library("form_validation");
    $this->load->library("encryptdecrypt");
    $this->load->model('API', 'api');
    $this->load->model('Getkey_model', 'getKey');

    // timezone
    date_default_timezone_set('Asia/Jakarta');
  }

  public function index()
  {
    $data['title'] = "Auth";
    $class['moreClass'] = "d-flex";
    $this->load->view('templates/header', $data);
    $this->load->view("auth/index");
    $this->load->view('templates/footer');
  }

  public function login()
  {
    $this->form_validation->set_rules(
      'nik',
      'NIK',
      'required',
      [
        'required' => 'NIK tidak boleh kosong !'
      ]
    );
    $this->form_validation->set_rules(
      'sandi',
      'Sandi',
      'required',
      [
        'required' => 'Sandi tidak boleh kosong !'
      ]
    );

    if ($this->form_validation->run() == FALSE) {
      $data['title'] = "Auth";
      $class['moreClass'] = "d-flex";
      $this->load->view('templates/header', $data);
      $this->load->view("auth/index");
      $this->load->view('templates/footer');
    } else {
      $post = $this->input->post();

      $url_sso_login = "http://10.10.10.16:9090/api/login";

      $params = [
        "nik" => $post["nik"],
        "password" => $post["sandi"],
        "app_key" => "W5o0FvjE0MDX0zTMQnUj"
      ];

      $result_sso = $this->api->post($url_sso_login, $params);

      if ($result_sso['success'] == true) {
        // create session userdata
        // $data_session = $result_sso['user'];
        $this->session->set_userdata($result_sso['user']);
      } else {
        $this->session->set_flashdata('message', '
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
        ' . $result_sso['message'] . '
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>  
        ');

        redirect('auth');
        die;
      }

      redirect('dashboard');
    }
  }

  public function log_out()
  {
    $this->session->sess_destroy();
    redirect('auth');
  }
}
