<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    // jika tidak ada session
    if (!$this->session->userdata('nik')) {
      redirect('auth');
    }

    date_default_timezone_set('Asia/Jakarta');

    $this->load->model('API', 'api');
    $this->load->model('Getkey_model', 'getKey');
    $this->load->model('Report_model', 'report');
  }

  public function index()
  {
    // get kode call report

    if ($this->input->post()) {
      $post = $this->input->post();
      // $status_id = $post['status_id'];
      $initial_of_competence = $post['initial_of_competence'];
      $business_unit_id = $post['business_unit_id'];
      $module_id = $post['module_id'];
      $start_date = $post['start_date'];
      $end_date = $post['end_date'];

      $params = [
        "status_id" => "",
        "initial_of_competence" => $initial_of_competence,
        "business_unit_id" => $business_unit_id,
        "module_id" => $module_id,
        "start_date" => $start_date,
        "end_date" => $end_date . " 23:59:59.999"
      ];
    } else {
      $params = [
        "status_id" => "",
        "initial_of_competence" => null,
        "business_unit_id" => null,
        "module_id" => null,
        "start_date" => date('Y-m-d'),
        "end_date" => date('Y-m-d') . ' 23:59:59.999'
      ];
    }

    $data["status_id"] = $params['status_id'];
    $data["initial_of_competence"] = $params['initial_of_competence'];
    $data["business_unit_id"] = $params['business_unit_id'];
    $data["module_id"] = $params['module_id'];
    $data["start_date"] = $params['start_date'];
    $data["end_date"] = $params['end_date'];

    $data['getReport'] = $this->report->getDataParams('Elearning/eLearning_get_call_report/', $params);

    if ($data['getReport']['status'] == true) {
      $data['getReportHeader'] = array_keys($data['getReport']['result'][0]);
      $data['getReportData'] = $data['getReport']['result'];
    } else {
      $data['getReportData'] = null;
      $data['getReportMessage'] = $data['getReport']['result'];
    }

    // get module
    $data['getModule'] = $this->report->getAll('Elearning/eLearning_get_module/');
    if ($data['getModule']['status'] == true) {
      $data['getModuleData'] = $data['getModule']['result'];
    } else {
      $data['getModuleData'] = null;
    }

    // get business unit
    $data['getBusinessUnit'] = $this->report->getAll('Elearning/eLearning_get_business_unit/');
    if ($data['getBusinessUnit']['status'] == true) {
      $data['getBusinessUnitData'] = $data['getBusinessUnit']['result'];
    } else {
      $data['getBusinessUnitData'] = null;
    }

    // get competence
    $data['getCompetence'] = $this->report->getAll('Elearning/eLearning_get_competence/');
    if ($data['getCompetence']['status'] == true) {
      $data['getCompetenceData'] = $data['getCompetence']['result'];
    } else {
      $data['getCompetenceData'] = null;
    }

    // var_dump($params);

    $data['title'] = "Report";
    $class['moreClass'] = "d-flex";
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar');
    $this->load->view('templates/wrapper', $class);
    $this->load->view('templates/sidebar');
    $this->load->view('templates/content');
    $this->load->view('content/report');
    $this->load->view('templates/end-content');
    $this->load->view('templates/end-wrapper');
    $this->load->view('templates/footer');
  }
}
