<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // jika tidak ada session
        if (!$this->session->userdata('nik')) {
            redirect('auth');
        }
    }

    public function index()
    {
        $data['title'] = "Users";
        $class['moreClass'] = "d-flex";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/topbar');
        $this->load->view('templates/wrapper', $class);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/content');
        $this->load->view('content/users');
        $this->load->view('templates/end-content');
        $this->load->view('templates/end-wrapper');
        $this->load->view('templates/footer');
    }
}
