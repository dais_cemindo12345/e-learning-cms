<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>scss/custom.css">

    <!-- datatables -->
    <link rel="stylesheet" href="<?= base_url() ?>node_modules/bootstrap/dataTables.bootstrap5.min.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?= base_url() ?>assets/web/image/logo-kpn.png" type="image/x-icon">

    <title><?= $title; ?></title>
</head>

<body class="bg-c1">