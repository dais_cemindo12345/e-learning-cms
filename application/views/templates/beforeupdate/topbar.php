<nav class="topbar navbar navbar-light bg-white p-0 shadow-sm">
  <div class="d-flex justify-content-between align-items-center w-100">
    <div class="btnMenu-and-image d-flex justify-content-start align-items-center">
      <div class="btnMenu">
        <a href="#" id="menuBtn">
          <img src="<?= base_url() ?>assets/web/icon/menu.svg" alt="menu">
        </a>
      </div>
      <div class="logo px-4">
        <img src="<?= base_url() ?>assets/web/image/logo.svg" alt="logo" height="40">
      </div>
    </div>
    <div class="menu d-flex justify-content-end align-items-center">
      <!-- <div class="notif ms-5">
        <a href="#" class="ms-3 text-decoration-none text-c9">
          <img src="<//?= base_url() ?>assets/web/icon/textsms.svg" alt="textsms">
        </a>
        <a href="#" class="ms-3 text-decoration-none text-c9">
          <img src="<//?= base_url() ?>assets/web/icon/notifications.svg" alt="notifications">
        </a>
      </div> -->
      <a href="#" class="ms-5 img-profile">
        <div class="img-placed rounded-circle" style="background-image: url(<?= base_url() ?>assets/web/image/profile.jfif);"></div>
      </a>
      </>
    </div>
</nav>