<div id="sidebar" class="w-25 vh-100 bg-white sidebar">
  <div class="wrapp-menu">
    <a href="<?= base_url() ?>" class="<?= ($this->uri->segment(1) == 'dashboard' || $this->uri->segment(1) == '' ? 'active' : '') ?> menu-list text-decoration-none d-flex justify-content-start align-items-center rounded-3">
      <div class="logo me-3">
        <img src="<?= base_url() ?>assets/web/icon/dashboard.svg" alt="dashboard">
      </div>
      <div class="text-c9 ubuntu">Dashboard</div>
    </a>
    <hr class="dropdown-divider text-light my-3">
    <a href="<?= base_url() ?>kelas" class="<?= ($this->uri->segment(1) == 'kelas' ? 'active' : '') ?> menu-list text-decoration-none d-flex justify-content-start align-items-center rounded-3">
      <div class="logo me-3">
        <img src="<?= base_url() ?>assets/web/icon/school.svg" alt="school">
      </div>
      <div class="text-c9 ubuntu">Kelas</div>
    </a>
    <a href="<?= base_url() ?>report" class="<?= ($this->uri->segment(1) == 'report' ? 'active' : '') ?> menu-list text-decoration-none d-flex justify-content-start align-items-center">
      <div class="logo me-3">
        <img src="<?= base_url() ?>assets/web/icon/Book.svg" alt="Book">
      </div>
      <div class="text-c9 ubuntu">Report</div>
    </a>
  </div>
</div>