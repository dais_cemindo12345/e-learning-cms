<nav class="topbar navbar navbar-light bg-white p-0 shadow-sm">
  <div class="d-flex justify-content-between align-items-center w-100">
    <div class="btnMenu-and-image d-flex justify-content-start align-items-center">
      <div class="btnMenu">
        <a href="#" id="menuBtn">
          <img src="<?= base_url() ?>assets/web/icon/menu.svg" alt="menu">
        </a>
      </div>
      <div class="logo px-4">
        <img src="<?= base_url() ?>assets/web/image/logo.svg" alt="logo" height="40">
      </div>
    </div>
    <div class="menu d-flex justify-content-end align-items-center">
      <!-- <div class="notif ms-5">
        <a href="#" class="ms-3 text-decoration-none text-c9">
          <img src="<//?= base_url() ?>assets/web/icon/textsms.svg" alt="textsms">
        </a>
        <a href="#" class="ms-3 text-decoration-none text-c9">
          <img src="<//?= base_url() ?>assets/web/icon/notifications.svg" alt="notifications">
        </a>
      </div> -->
      <!-- <a href="#" class="ms-5 img-profile">
        <div class="img-placed rounded-circle" style="background-image: url(<?= base_url() ?>assets/web/image/profile.jfif);"></div>
      </a> -->
      <div class="dropdown">
        <a href="#" class="ms-5 d-flex justify-content-center align-items-center text-decoration-none" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
          <div class="name_display me-3">
            <div class="text-dark"><?= $this->session->userdata('name'); ?></div>
            <div class="text-secondary" style="font-size: 10px;"><?= $this->session->userdata('role_display_name'); ?></div>
          </div>
          <div class="img-placed rounded-circle">
            <img class="rounded-circle" src="<?= base_url() ?>assets/web/image/profile.jfif" alt="profile" width="40" height="40">
          </div>
        </a>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
          <li><a class="dropdown-item" href="<?= base_url() ?>auth/log_out" onclick="return confirm('Apakah anda yakin ingin keluar ?')">Log out</a></li>
        </ul>
      </div>
    </div>
</nav>