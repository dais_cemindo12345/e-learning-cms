<div class="auth">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6 col-lg-4 vh-100 mx-auto d-flex align-items-center flex-column p-5">
        <div class="mb-4">
          <img src="<?= base_url() ?>assets/web/image/logo-kpn.png" alt="Logo" width="40">
        </div>

        <div class="mb-4">
          <img src="<?= base_url() ?>assets/web/image/logo.svg" alt="Logo" width="150">
        </div>

        <?= $this->session->flashdata('message'); ?>

        <form action="<?= base_url() ?>auth/login" method="POST">
          <div class="mb-3">
            <input type="text" class="form-control" name="nik" id="nik" placeholder="Masukan NIK" value="<?= set_value('nik'); ?>">
            <?php echo form_error('nik', '<small class="text-warning error">', '</small>'); ?>
          </div>
          <div class="mb-3">
            <input type="password" class="form-control" name="sandi" id="sandi" placeholder="Masukan sandi">
            <?php echo form_error('sandi', '<small class="text-warning error">', '</small>'); ?>
          </div>
          <div class="d-grid">
            <button type="submit" class="btn btn-primary">Masuk</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>