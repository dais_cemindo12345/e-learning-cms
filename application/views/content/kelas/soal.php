<div class="wrap-soal">
    <div class="header d-flex justify-content-between align-items-center mb-4">
        <h5 class="title fw-bold ubuntu">Buat soal - <?= $dataClass['title'] ?></h5>
        <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#tambahSoal">Tambah Soal</button>
    </div>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url() ?>kelas">Kelas</a></li>
            <li class="breadcrumb-item active" aria-current="page">Soal</li>
        </ol>
    </nav>

    <?= $this->session->flashdata('message'); ?>

    <div id="question-list">

        <?php if ($dataShowQuestion == true) : ?>

            <?php foreach ($showQuestion as $question) : ?>

                <?php if ($question['questionType'] == "mc") : ?>

                    <?php $answerQuestion = $this->kelas->getDataById('Elearning/eLearning_get_option_question_by_question_code/', $question['questionCode']); ?>

                    <?php if ($answerQuestion['status'] == true) : ?>
                        <?php $answerQuestion = $answerQuestion['result']; ?>
                    <?php endif; ?>

                    <div class="wrapp-question-view bg-white p-4 border-5 border-top border-primary rounded shadow-sm mb-3">
                        <div class="d-flex justify-content-between align-items-start mb-4">
                            <div class="question-view w-75 fw-bold"><?= $question['question'] ?></div>
                            <div class="question-point-view w-25 text-end"><?= $question['questionPoint'] ?> Poin</div>
                        </div>

                        <?php foreach ($answerQuestion as $aq) : ?>
                            <?php if ($aq['correctAnswer'] == 1 || $aq['correctAnswer'] == true) : ?>
                                <div class="d-block bg-mc4 p-3 rounded-3">
                                    <input class="form-check-input me-3" type="radio" name="flexRadioDisabled" id="flexRadioDisabled" disabled>
                                    <span><?= $aq['option'] ?></span>
                                </div>
                            <?php endif; ?>

                            <?php if ($aq['correctAnswer'] == 0 || $aq['correctAnswer'] == false) : ?>
                                <div class="d-block p-3 rounded-3">
                                    <input class="form-check-input me-3" type="radio" name="flexRadioDisabled" id="flexRadioDisabled" disabled>
                                    <span><?= $aq['option'] ?></span>
                                </div>
                            <?php endif; ?>

                        <?php endforeach; ?>

                        <a href="<?= base_url() ?>kelas/delete_question?qc=<?= $this->encryptdecrypt->dais_encrypt($question['questionCode']); ?>&c=<?= $this->input->get('c'); ?>" class="btn btn-white text-danger mt-3" onclick="return confirm('Apakah anda yakin ingin menghapus soal ini ?')"><i class="bi bi-trash me-2"></i>Hapus soal</a>

                    </div>

                <?php else : ?>
                    <div class="wrapp-question-view bg-white p-4 border-5 border-top border-primary rounded shadow-sm mb-3">
                        <div class="d-flex justify-content-between align-items-start mb-4">
                            <div class="question-view w-75 fw-bold"><?= $question['question'] ?></div>
                            <div class="question-point-view w-25 text-end"><?= $question['questionPoint'] ?> Poin</div>
                        </div>
                        <hr class="dropdown-divider my-3" style="border-top: .1px solid rgba(0, 0, 0, 0.15) !important; height: 0px !important">
                        <div class="text text-muted p-3">Jawaban Esai ...</div>

                        <a href="<?= base_url() ?>kelas/delete_question?qc=<?= $this->encryptdecrypt->dais_encrypt($question['questionCode']); ?>&c=<?= $this->input->get('c'); ?>" class="btn btn-white text-danger mt-3" onclick="return confirm('Apakah anda yakin ingin menghapus soal ini ?')"><i class="bi bi-trash me-2"></i>Hapus soal</a>
                    </div>


                <?php endif; ?>

            <?php endforeach; ?>

        <?php endif; ?>

    </div>


    <!-- Modal -->
    <div class="modal fade" id="tambahSoal" tabindex="-1" aria-labelledby="tambahSoalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tambahSoalLabel">Tambah Soal</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="form-soal" action="<?= base_url() ?>kelas/add_soal" method="post">
                    <div class="modal-body">
                        <input type="hidden" data-name="id-class" data-url="<?= base_url() ?>kelas/add_soal" id="id-class" value="<?= $_GET['c']; ?>">
                        <div class="d-flex justify-content-between align-items-start">
                            <textarea class="form-control me-3 w-75" type="text" placeholder="Pertanyaan" name="pertanyaan" id="pertanyaan" rows="1"></textarea>
                            <select class="tipe-soal form-select w-25" name="tipe-soal" id="tipe-soal" aria-label="Default select example">
                                <option value="mc" selected>Multiple choice</option>
                                <option value="p">Paragraph</option>
                            </select>
                        </div>
                        <hr class="dropdown-divider my-3" style="border-top: .1px solid rgba(0, 0, 0, 0.15) !important; height: 0px !important">
                        <div class="answer">
                            <div class="mc d-block">
                                <div class="options">
                                    <div class="row">
                                        <div class="col-2">
                                            <div class="my-3 d-flex align-items-center">
                                                <input class="point-form form-control me-3" type="number" name="point" id="point" placeholder="0">
                                                <span>poin</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-row align-items-center mb-3">
                                        <input class="form-check-input me-3" type="checkbox" name="flexCheckChecked">
                                        <input class="mc-form form-control border-0 border-bottom border-1 me-3" type="text" name="mc" id="mc" placeholder="Opsi">
                                    </div>
                                </div>
                                <a href="#" class="add-option btn btn-link text-primary text-decoration-none fw-bold">+ Tambah opsi</a>
                            </div>
                            <div class="p d-none">
                                <div class="col-2">
                                    <div class="my-3 d-flex align-items-center">
                                        <input class="point-form form-control me-3" type="number" name="point_p" id="point_p" placeholder="0">
                                        <span>poin</span>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <p class="text-secondary">Teks jawaban panjang</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="button" class="btn btn-primary" id="submit">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="<?= base_url() ?>js/soal.js"></script>