<div class="wrap-penugasan">
    <div id="alertAssignment" class="position-fixed mt-3" style="z-index: 2000;"></div>

    <div class="header d-flex justify-content-between align-items-center mb-4">
        <h5 class="title fw-bold ubuntu">Penugasan Kelas - <?= $classTitle; ?></h5>
    </div>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url() ?>kelas">Kelas</a></li>
            <li class="breadcrumb-item active" aria-current="page">Penugasan</li>
        </ol>
    </nav>


    <div id="assign-to" class="assign-to p-3 shadow my-3 rounded-3">
        <h5>Telah ditugaskan</h5>

        <?php if ($assignmentByClassId['status'] == false) : ?>
            <p class="text-secondary" id="assign-null">Belum ada karyawan yang di tugaskan</p>
        <?php else : ?>
            <?php foreach ($assignmentByClassId['result'] as $assignment) : ?>
                <div class="bg-mc5 p-2 d-inline-block rounded-2 align-items-center me-2 mb-2">
                    <span class="text-dark"><?= $assignment['employeeName'] ?></span>
                    <a href="#" role="button" data-url="<?= base_url() ?>" data-nik="<?= $assignment['employeeNIK'] ?>" data-kelas="<?= $this->encryptdecrypt->dais_encrypt($assignment['classId']); ?>" class="cancle-assignment"><img src="<?= base_url() ?>assets/web/icon/close_danger.svg" alt="delete"></a>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

    <form action="" method="post">
        <div class="input-group mb-3">
            <input type="text" class="form-control" id="search-data" placeholder="Cari data berdasarkan NIK atau Nama Lengkap" aria-label="Recipient's username" name="search_data_karyawan" aria-describedby="button-addon2">
            <button class="btn btn-primary" type="submit" name="search" id="button-addon2">Cari</button>
        </div>
    </form>

    <?php if (!empty($dataKaryawan)) : ?>
        <?php if ($dataKaryawanStatus == false) : ?>
            <h5 class="text-danger"><?= $dataKaryawan; ?></h5>
        <?php elseif ($dataKaryawanStatus == true) : ?>
            <div class="wrap-table-kelas bg-white rounded overflow-auto">
                <div class="header d-flex justify-content-between align-items-center bg-c9 p-3 rounded-top">
                    <div class="title ubuntu text-c1">Tabel karyawan</div>
                </div>
                <div class="body p-3 overflow-auto">
                    <table id="tableKelas" class="table table-striped table-bordered table-responsive py-3" style="width:100%">
                        <thead>
                            <tr>
                                <th></th>
                                <th>ID karyawan</th>
                                <th>NIK</th>
                                <th>Nama lengkap</th>
                                <th>Email</th>
                                <th>Email pribadi</th>
                                <th>Mulai bekerja</th>
                                <th>Jabatan</th>
                                <th>Departemen</th>
                                <th>Sub departemen</th>
                                <th>Nama Perusahaan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($dataKaryawan as $dk) : ?>
                                <tr>
                                    <td>
                                        <button class="assign btn btn-sm btn-primary" data-bs-toggle="tooltip" data-bs-placement="top" title="Tugaskan" data-url="<?= base_url() ?>" data-id="<?= $dk['id'] ?>" data-nik="<?= $dk['NIK'] ?>" data-nama="<?= $dk['Nama_Lengkap'] ?>" data-kelas="<?= $this->input->get('c'); ?>"><img src="<?= base_url() ?>assets/web/icon/person_add_white.svg" alt="" srcset=""></button>
                                    </td>
                                    <td><?= $dk['id'] ?></td>
                                    <td><?= $dk['NIK'] ?></td>
                                    <td><?= $dk['Nama_Lengkap'] ?></td>
                                    <td><?= $dk['Email'] ?></td>
                                    <td><?= $dk['Email_Pribadi'] ?></td>
                                    <td><?= $dk['Mulai_Bekerja'] ?></td>
                                    <td><?= $dk['Nama_Jabatan'] ?></td>
                                    <td><?= $dk['Nama_departemen'] ?></td>
                                    <td><?= $dk['Nama_Sub_Departemen'] ?></td>
                                    <td><?= $dk['Nama_Lkp_Perusahaan'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>

</div>

<!-- js -->
<script src="<?= base_url() ?>node_modules/jquery/dist/jquery-3.5.1.js"></script>
<script src="<?= base_url() ?>node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>node_modules/jquery/dist/dataTables.bootstrap5.min.js"></script>
<script src="<?= base_url() ?>js/datatables.js"></script>
<script src="<?= base_url() ?>js/penugasan.js"></script>