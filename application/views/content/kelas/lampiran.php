<div class="wrap-lampiran">
    <?= $this->session->flashdata('message'); ?>
    <?php if (!empty(validation_errors())) : ?>
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Data input tidak lengkap !</h4>
            <div class="validation-errors">
                <?= validation_errors(); ?>
                <hr>
                <p class="mb-0">Cek ulang form "Tambah kelas"</p>
            </div>
        </div>
    <?php endif; ?>
    <div class="header d-flex justify-content-between align-items-center mb-4">
        <h5 class="title fw-bold ubuntu"><?= $dataClass['title'] ?></h5>
        <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#tambahLampiran">Tambah lampiran</button>
    </div>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url() ?>kelas">Kelas</a></li>
            <li class="breadcrumb-item active" aria-current="page">Lampiran</li>
        </ol>
    </nav>

    <div class="file-uploaded">
        <?php if ($dataAttachmentClass['status'] == FALSE) : ?>
            <p>Belum ada data yang di upload</p>
        <?php else : ?>
            <?php foreach ($dataAttachmentClass['result'] as $attachment) :  ?>
                <div class="alert alert-primary" role="alert">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="title"><?= $attachment['title'] ?></div>
                        <div class="menu">
                            <a class="ms-2" href="<?= LOAD_IMAGE_ON_PUBLIC . $attachment['attachmentName'] ?>" target="_blank" data-bs-toggle="tooltip" data-bs-placement="left" title="Lihat"><img src="<?= base_url() ?>assets/web/icon/launch.svg" alt="launch.svg"></a>

                            <a class="ms-2" onclick="return confirm('Apakah anda yakin ingin menghapis data ini?')" href="<?= base_url() ?>kelas/deleteAttachment?d=<?= $this->encryptdecrypt->dais_encrypt($attachment['id']); ?>&f=<?= $this->encryptdecrypt->dais_encrypt($attachment['attachmentName']); ?>" data-bs-toggle="tooltip" data-bs-placement="left" title="Hapus"><img src="<?= base_url() ?>assets/web/icon/delete.svg" alt="launch.svg"></a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="tambahLampiran" tabindex="-1" aria-labelledby="tambahLampiranLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tambahLampiranLabel">Tambah Lampiran</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url() ?>kelas/add_attachment" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <input type="hidden" id="classId" name="classId" value="<?= $this->input->get('c'); ?>">
                    <div class="mb-3">
                        <label for="judul" class="form-label">Judul <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="judul" name="judul" required>
                        <?php echo form_error('judul', '<small class="error text-danger">', '</small>'); ?>
                    </div>
                    <div class="mb-3">
                        <div class="title">
                            <label for="judul" class="form-label">Pilih tipe lampiran</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="lampiran" id="lampiran_dokumen" value="dokumen" checked>
                            <label class="form-check-label" for="lampiran_dokumen">
                                Dokumen
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="lampiran" id="lampiran_gambar" value="gambar">
                            <label class="form-check-label" for="lampiran_gambar">
                                Gambar
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="lampiran" id="lampiran_video" value="video">
                            <label class="form-check-label" for="lampiran_video">
                                Video
                            </label>
                        </div>
                    </div>
                    <div class="mb-3">
                        <input class="form-control" type="file" id="formFile" name="attachment" required>
                        <div id="ekstensi">
                            <small class="text-danger">Tipe file : pdf dan maksimal file 2 MB</small>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>js/lampiran.js"></script>