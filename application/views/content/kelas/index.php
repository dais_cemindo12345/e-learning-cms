<div class="wrap-kelas">
    <?php if (!empty(validation_errors())) : ?>
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Data input tidak lengkap !</h4>
            <div class="validation-errors">
                <?= validation_errors(); ?>
                <hr>
                <p class="mb-0">Cek ulang form "Tambah kelas"</p>
            </div>
        </div>
    <?php endif; ?>
    <?= $this->session->flashdata('message'); ?>
    <div class="wrap-table-kelas bg-white rounded">
        <div class="header d-flex justify-content-between align-items-center bg-c9 p-3 rounded-top">
            <div class="title ubuntu text-c1">Tabel kelas</div>
            <div class="btnAct"><button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#tambahKelas">Tambah kelas</button></div>
        </div>
        <div class="body p-3">
            <table id="tableKelas" class="table table-striped table-bordered table-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>Gambar</th>
                        <th>Judul</th>
                        <th>Tanggal Pembuatan</th>
                        <th>Status</th>
                        <th>Menu</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($getClass['status'] == TRUE) : ?>
                        <?php foreach ($getClass['result'] as $getClass) : ?>
                            <tr>
                                <td>
                                    <?php if ($getClass['imageStatus'] == 0) : ?>
                                        <div class="d-flex flex-column justify-content-center align-items-center">
                                            <div>
                                                <small>Tidak ada gambar</small>
                                            </div>
                                            <a href="<?= base_url() ?>kelas/reupload?fn=<?= $this->encryptdecrypt->dais_encrypt($getClass['pictureName']); ?>&fp=<?= $this->encryptdecrypt->dais_encrypt('C:/xampp/htdocs/10.36/e-learning-CMS/uploads/'); ?>" class="btn btn-sm btn-primary">Upload ulang</a>
                                        </div>
                                    <?php else : ?>
                                        <div class="wrap-img-table rounded mx-auto">
                                            <img class="img-table" src="<?= LOAD_IMAGE_ON_PUBLIC . $getClass['pictureName'] ?>" alt="$getClass['pictureName']">
                                        </div>
                                    <?php endif; ?>
                                </td>
                                <td><?= $getClass['title'] ?></td>
                                <td><?= $getClass['dateCreated'] ?></td>
                                <td>
                                    <?php if ($getClass['statusName'] == 'Draft') : ?>
                                        <div class="status text-danger"><?= $getClass['statusName'] ?></div>
                                    <?php else : ?>
                                        <div class="status text-success"><?= $getClass['statusName'] ?></div>
                                    <?php endif; ?>
                                </td>
                                <td class="text-center">
                                    <div class="dropstart">
                                        <a class="text-c9" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                            <img src="<?= base_url() ?>assets/web/icon/list.svg" alt="list">
                                        </a>

                                        <ul class="dropdown-menu me-3 px-4 py-3" aria-labelledby="dropdownMenuLink">
                                            <li><a href="#" data-bs-toggle="modal" id="ubah" data-url="<?= base_url() ?>kelas/get_class_by_id_for_js/" data-id="<?= $this->encryptdecrypt->dais_encrypt($getClass['id']); ?>" data-bs-target="#ubahData" class="ubah dropdown-item p-2 text-decoration-none text-c9"><img src="<?= base_url() ?>assets/web/icon/create.svg" alt="edit" class="me-3"> Ubah</a></li>
                                            <li><a href="<?= base_url() ?>kelas/lampiran?c=<?= $this->encryptdecrypt->dais_encrypt($getClass['id']); ?>" class="dropdown-item p-2 text-decoration-none text-c9"><img src="<?= base_url() ?>assets/web/icon/attach_file.svg" alt="attach_file" class="me-3"> Lampiran</a></li>
                                            <li><a href="<?= base_url() ?>kelas/soal?c=<?= $this->encryptdecrypt->dais_encrypt($getClass['id']); ?>" class="dropdown-item p-2 text-decoration-none text-c9"><img src="<?= base_url() ?>assets/web/icon/playlist_add_check.svg" alt="attach_file" class="me-3"> Soal</a></li>
                                            <li><a href="<?= base_url() ?>kelas/penugasan?c=<?= $this->encryptdecrypt->dais_encrypt($getClass['id']); ?>&t=<?= $getClass['title'] ?>" class="dropdown-item p-2 text-decoration-none text-c9"><img src="<?= base_url() ?>assets/web/icon/verified_user.svg" alt="attach_file" class="me-3"> Penugasan</a></li>
                                            <li>
                                                <hr class="dropdown-divider text-c2">
                                            </li>
                                            <?php if ($getClass['statusId'] == 2) : ?>
                                                <li><a href="#" class="dropdown-item p-2 text-decoration-none disabled" aria-disabled="true"><img src="<?= base_url() ?>assets/web/icon/publish_disabled.svg" alt="publish" class="me-3"> Publish</a></li>
                                            <?php else : ?>
                                                <li><a href="<?= base_url() ?>kelas/publish?c=<?= $this->encryptdecrypt->dais_encrypt($getClass['id']); ?>" class="dropdown-item p-2 text-decoration-none text-mc6"><img src="<?= base_url() ?>assets/web/icon/publish.svg" alt="publish" class="me-3"> Publish</a></li>
                                            <?php endif; ?>
                                            <li><a href="<?= base_url() ?>kelas/delete_class?d=<?= $this->encryptdecrypt->dais_encrypt($getClass['id']); ?>" onclick="return confirm('Menghapus kelas juga akan menghapus data lampiran terkait dan karyawan yang di tugaskan. Hapus kelas ini?')" class="dropdown-item p-2 text-decoration-none text-mc1"><img src="<?= base_url() ?>assets/web/icon/delete.svg" alt="delete" class="me-3"> Hapus</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal tambah kelas -->
<div class="modal fade" id="tambahKelas" tabindex="-1" aria-labelledby="tambahKelasLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tambahKelasLabel">Tambah Kelas</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url() ?>kelas/tambah" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="final-code mb-4">
                        <h5 class="fw-bolder">Buat kode</h5>
                        <div class="row">
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="kode_kopetensi" class="form-label">Kode kopetensi <span class="text-danger">*</span></label>
                                    <select class="form-select" id="kode_kopetensi" name="kode_kopetensi" aria-label="Default select example">
                                        <?php if ($getCopetence['status'] == true) : ?>
                                            <?php foreach ($getCopetence['result'] as $copetence) : ?>
                                                <option value="<?= $copetence['initialOfCompetence'] ?>" selected>
                                                    <div class="inisial-kode p-2 bg-brand text-white"><?= $copetence['initialOfCompetence'] ?> - </div>
                                                    <div class="nama-kode"><?= $copetence['competenceName'] ?></div>
                                                </option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="kode_unit_bisnis" class="form-label">Kode unit bisnis <span class="text-danger">*</span></label>
                                    <select class="form-select" id="kode_unit_bisnis" name="kode_unit_bisnis" aria-label="Default select example">
                                        <?php if ($getBusinessUnit['status'] == true) : ?>
                                            <?php foreach ($getBusinessUnit['result'] as $businessUnit) : ?>
                                                <option value="<?= $businessUnit['businessUnitId'] ?>" selected>
                                                    <div class="inisial-kode p-2 bg-brand text-white"><?= $businessUnit['businessUnitId'] ?> - </div>
                                                    <div class="nama-kode"><?= $businessUnit['businessUnitName'] ?></div>
                                                </option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <label for="modul_id" class="form-label">Modul id <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="modul_id" name="modul_id" maxlength="3" required>
                            </div>
                            <div class="col-4">
                                <label for="kode_kelas" class="form-label">Kode kelas <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="kode_kelas" name="kode_kelas" maxlength="2" required>
                            </div>
                            <div class="col-4">
                                <label for="kode_sub_kelas" class="form-label">Kode sub kelas <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="kode_sub_kelas" name="kode_sub_kelas" maxlength="1" required>
                            </div>
                        </div>
                        <div class="my-3">
                            <a href="#" id="cek-kode" class="btn btn-outline-info btn-small" data-url="<?= base_url() ?>kelas/check_final_code/">Cek kode</a>
                        </div>
                        <div class="d-inline-block d-none">
                            <div class="d-inline me-3" id="text-final-code"></div>
                            <div class="d-inline" id="status-final-code"></div>
                        </div>
                    </div>
                    <div class="data-class mb-4">
                        <h5 class="fw-bolder">Data kelas</h5>
                        <div class="mb-3">
                            <label for="nama_modul" class="form-label">Nama modul <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="nama_modul" name="nama_modul" value="<?= set_value('nama_modul'); ?>" required>
                            <?php echo form_error('nama_modul', '<small class="error text-danger">', '</small>'); ?>
                        </div>
                        <div class="mb-3">
                            <label for="judul" class="form-label">Judul <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="judul" name="judul" value="<?= set_value('judul'); ?>" required>
                            <?php echo form_error('judul', '<small class="error text-danger">', '</small>'); ?>
                        </div>
                        <div class="mb-3">
                            <label for="judul_sub_kelas" class="form-label">Judul sub kelas <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="judul_sub_kelas" name="judul_sub_kelas" value="<?= set_value('judul_sub_kelas'); ?>" required>
                            <?php echo form_error('judul_sub_kelas', '<small class="error text-danger">', '</small>'); ?>
                        </div>
                        <div class="mb-3">
                            <label for="tgl_aktif" class="form-label">Tanggal aktif <span class="text-danger">*</span></label>
                            <input type="datetime-local" class="form-control" id="tgl_aktif" name="tgl_aktif" value="<?= set_value('tgl_aktif'); ?>" required>
                            <?php echo form_error('tgl_aktif', '<small class="error text-danger">', '</small>'); ?>
                        </div>
                        <div class="mb-3">
                            <label for="tgl_kadaluarsa" class="form-label">Tanggal kadaluarsa <span class="text-danger">*</span></label>
                            <input type="datetime-local" class="form-control" id="tgl_kadaluarsa" name="tgl_kadaluarsa" value="<?= set_value('tgl_kadaluarsa'); ?>" required>
                            <?php echo form_error('tgl_kadaluarsa', '<small class="error text-danger">', '</small>'); ?>
                        </div>
                        <div class="mb-3">
                            <label for="gambar_kelas" class="form-label">Gambar Kelas <span class="text-danger">*</span></label>
                            <input class="form-control" type="file" id="gambar_kelas" name="gambar_kelas" required>
                            <small class="text-secondary">Maksimal ukuran file 2MB dan ukuran gambar 1024 &#x2715; 786 px</small>
                        </div>
                        <div class="mb-3">
                            <label for="tujuan_pelatihan" class="form-label">Tujuan Pelatihan <span class="text-danger">*</span></label>
                            <textarea class="form-control" id="tujuan_pelatihan" name="tujuan_pelatihan" rows="3" required><?= set_value('tujuan_pelatihan'); ?></textarea>
                            <?php echo form_error('tujuan_pelatihan', '<small class="error text-danger">', '</small>'); ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Ubah Data -->
<div class="modal fade" id="ubahData" tabindex="-1" aria-labelledby="ubahDataLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ubahDataLabel">Ubah Data</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url() ?>kelas/update_class" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <input type="hidden" id="id_class" name="id_class">
                    <div class="mb-3">
                        <label for="ubah_judul" class="form-label">Judul <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="ubah_judul" name="ubah_judul">
                        <?php echo form_error('ubah_judul', '<small class="error text-danger">', '</small>'); ?>
                    </div>
                    <div class="mb-3">
                        <label for="ubah_tgl_aktif" class="form-label">Tanggal aktif <span class="text-danger">*</span></label>
                        <input type="datetime-local" class="form-control" id="ubah_tgl_aktif" name="ubah_tgl_aktif" value="<?= set_value('ubah_tgl_aktif'); ?>" required>
                        <?php echo form_error('ubah_tgl_aktif', '<small class="error text-danger">', '</small>'); ?>
                    </div>
                    <div class="mb-3">
                        <label for="ubah_tgl_kadaluarsa" class="form-label">Tanggal kadaluarsa <span class="text-danger">*</span></label>
                        <input type="datetime-local" class="form-control" id="ubah_tgl_kadaluarsa" name="ubah_tgl_kadaluarsa" value="<?= set_value('ubah_tgl_kadaluarsa'); ?>" required>
                        <?php echo form_error('ubah_tgl_kadaluarsa', '<small class="error text-danger">', '</small>'); ?>
                    </div>
                    <div class="mb-3">
                        <!-- old image -->
                        <label for="ubah_gambar_kelas" class="form-label">Gambar Kelas <span class="text-danger">*</span></label>
                        <img id="old_image_view" class="img-thumbnail mb-2">
                        <input type="hidden" id="old_image" name="old_image">
                        <input class="form-control" type="file" id="ubah_gambar_kelas" name="ubah_gambar_kelas">
                        <small class="text-secondary">Klik choose file jika ingin mengganti gambar</small>
                        <div>
                            <small class="text-secondary">Maksimal ukuran file 2MB dan ukuran gambar 1024 &#x2715; 786 px</small>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="ubah_tujuan_pelatihan" class="form-label">Tujuan Pelatihan <span class="text-danger">*</span></label>
                        <textarea class="form-control" id="ubah_tujuan_pelatihan" name="ubah_tujuan_pelatihan" rows="3"></textarea>
                        <?php echo form_error('ubah_tujuan_pelatihan', '<small class="error text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link text-decoration-none text-secondary" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan perubahan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- js -->
<script src="<?= base_url() ?>node_modules/jquery/dist/jquery-3.5.1.js"></script>
<script src="<?= base_url() ?>node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>node_modules/jquery/dist/dataTables.bootstrap5.min.js"></script>
<script src="<?= base_url() ?>js/datatables.js"></script>
<script src="<?= base_url() ?>js/ubahDataClass.js"></script>