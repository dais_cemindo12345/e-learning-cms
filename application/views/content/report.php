<div class="container">
  <div class="mt-3">
    <!-- <h5>Report</h5> -->
    <div class="mb-4 p-3 bg-white rounded shadow-sm">
      <form action="<?= base_url() ?>report" method="post">
        <h3 class="title mb-3">Filter data</h3>
        <div class="d-flex align-align-items-stretch mb-3">
          <div class="me-3 flex-fill">
            <label for="initial_of_competence" class="form-label">Competence</label>
            <select class="form-select" aria-label="Default select example" name="initial_of_competence" id="initial_of_competence">
              <option value="" selected>All</option>
              <?php foreach ($getCompetenceData as $competence) : ?>
                <option value="<?= $competence['initialOfCompetence'] ?>" <?php if ($initial_of_competence == $competence['initialOfCompetence']) : ?> selected <?php endif; ?>>(<?= $competence['initialOfCompetence'] ?>) <?= $competence['competenceName'] ?></option>
              <?php endforeach; ?>
            </select>
          </div>

          <div class="me-3 flex-fill">
            <label for="business_unit_id" class="form-label">Business unit</label>
            <select class="form-select" aria-label="Default select example" name="business_unit_id" id="business_unit_id">
              <option value="" selected>All</option>
              <?php foreach ($getBusinessUnitData as $businessUnit) : ?>
                <option value="<?= $businessUnit['businessUnitId'] ?>" <?php if ($business_unit_id == $businessUnit['businessUnitId']) : ?> selected <?php endif; ?>><?= $businessUnit['businessUnitName'] ?></option>
              <?php endforeach; ?>
            </select>
          </div>

          <div class="me-3 flex-fill">
            <label for="module_id" class="form-label">Module</label>
            <select class="form-select" aria-label="Default select example" name="module_id" id="module_id">
              <option value="" selected>All</option>
              <?php foreach ($getModuleData as $module) : ?>
                <option value="<?= $module['moduleId'] ?>" <?php if ($module_id == $module['moduleId']) : ?> selected <?php endif; ?>><?= $module['moduleName'] ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>

        <div class="mb-3">
          <label for="reportrange" class="form-label">Date Created</label>
          <!-- <input type="date" class="form-control" id="date_created" name="date_created" value="2022-03-19"> -->
          <div id="reportrange" class="form-control">
            <i class="fa fa-calendar"></i>&nbsp;
            <span></span> <i class="fa fa-caret-down"></i>
            <input type="hidden" id="start_date" name="start_date" value="<?= $start_date; ?>">
            <input type="hidden" id="end_date" name="end_date" value="<?= $end_date; ?>">
          </div>
        </div>

        <div class="m-0">
          <button type="submit" class="btn btn-brand">Set filter</button>
        </div>
      </form>
    </div>

    <div class="wrap-table-kelas bg-white rounded">
      <div class="header d-flex justify-content-between align-items-center bg-c9 p-3 rounded-top">
        <div class="title ubuntu text-c1">Tabel Report</div>
        <!-- <div class="btnAct"><button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#tambahKelas">Tambah kelas</button></div> -->
      </div>
      <div class="body p-3 overflow-auto">
        <div class="d-flex flex-row justify-content-end align-items-center mb-3">
          <span class="me-4">Export to </span>
          <div id="example_wrapper2"></div>
        </div>
        <table id="tableReport" class="table table-striped table-bordered table-responsive" style="width:100%">
          <?php if ($getReportData == null) : ?>
            <p class="text-danger"><?= $getReportMessage; ?></p>
          <?php else : ?>
            <thead>
              <tr>
                <?php foreach ($getReportHeader as $header) : ?>
                  <th scope="col"><?= $header ?></th>
                <?php endforeach; ?>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($getReportData as $data) : ?>
                <tr>
                  <td><?= $data['Class Status'] ?></td>
                  <td><?= $data['Class Code'] ?></td>
                  <td><?= $data['trainingObjectives'] ?></td>
                  <td><?= $data['businessUnitName'] ?></td>
                  <td><?= $data['CompetenceName'] ?></td>
                  <td><?= $data['moduleName'] ?></td>
                  <td><?= $data['class name'] ?></td>
                  <td><?= $data['subClassName'] ?></td>
                  <td><?= $data['Trainee ID'] ?></td>
                  <td><?= $data['Trainee Name'] ?></td>
                  <td><?= $data['statusName'] ?></td>
                  <td><?= $data['Score'] ?></td>
                  <td><?= $data['processingTime'] ?></td>
                  <td><?= $data['submitDate'] ?></td>
                  <td><?= $data['datecreated'] ?></td>
                  <td><?= $data['expiredClass'] ?></td>
                  <td><?= $data['Create By'] ?></td>
                  <td><?= $data['employeeName'] ?></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          <?php endif; ?>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- css -->
<script src="<?= base_url() ?>node_modules/datatables.net/css/buttons.dataTables.min.css"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>scss/daterangepicker.css" />

<!-- js -->
<script src="<?= base_url() ?>node_modules/jquery/dist/jquery-3.5.1.js"></script>

<script src="<?= base_url() ?>node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>node_modules/datatables.net/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>node_modules/datatables.net/js/jszip.min.js"></script>
<script src="<?= base_url() ?>node_modules/datatables.net/js/pdfmake.min.js"></script>
<script src="<?= base_url() ?>node_modules/datatables.net/js/vfs_fonts.js"></script>
<script src="<?= base_url() ?>node_modules/datatables.net/js/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>node_modules/datatables.net/js/buttons.print.min.js"></script>
<script src="<?= base_url() ?>node_modules/jquery/dist/dataTables.bootstrap5.min.js"></script>
<script src="<?= base_url() ?>js/datatables.js"></script>

<script type="text/javascript" src="<?= base_url() ?>js/moment.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/daterangepicker.min.js"></script>
<script type="text/javascript">
  $(function() {
    let startDate = String($('#start_date').val());
    let endDate = String($('#end_date').val());

    if (startDate == "" && endDate == "") {
      var start = moment().startOf('month');
      var end = moment().endOf('month');
    } else {
      var start = moment(startDate);
      var end = moment(endDate);
    }

    function cb(start, end) {
      $('#reportrange span').html(start.format('YYYY-MM-DD') + '  -  ' + end.format('YYYY-MM-DD'));
      $('#start_date').val(start.format('YYYY-MM-DD'));
      $('#end_date').val(end.format('YYYY-MM-DD'));
    }

    $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
    }, cb);

    cb(start, end);
  });
</script>