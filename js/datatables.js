$(document).ready(function() {
    $('#tableKelas').DataTable();

    var table = $('#tableReport').DataTable( {
        // lengthChange: false,
        "serverSize": true,
        "deferRender": true,
        buttons: [ 'copy', 'excel', 'pdf']
    } );

    table.buttons().container()
    // .appendTo( '#example_wrapper .col-md-6:eq(0)' );
    .appendTo( '#example_wrapper2' );

    $('button.dt-button').addClass('btn btn-secondary');
} );