const btnUbah = document.querySelectorAll('.ubah');

const getDataClassById = (params) => {

    // data
    const dataUrl = params.url;
    const dataId = params.id;

    const xhttp = new XMLHttpRequest();

    xhttp.open("POST", dataUrl, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // Access the onreadystatechange event for the XMLHttpRequest object
    xhttp.onreadystatechange = function() {

        if(xhttp.readyState == 4 && xhttp.status == 200) {
 
            const return_data = JSON.parse(xhttp.response);
            console.log(return_data);

            const activeClass = return_data.dataClass.activeClass
            const expiredClass = return_data.dataClass.expiredClass

            document.getElementById('id_class').setAttribute("value", return_data.idEncrypt);
            document.getElementById('ubah_judul').setAttribute("value", return_data.dataClass.title);
            document.getElementById('ubah_tgl_aktif').setAttribute("value", activeClass.replace(/\s/g, 'T'));
            document.getElementById('ubah_tgl_kadaluarsa').setAttribute("value", expiredClass.replace(/\s/g, 'T'));
            document.getElementById('old_image_view').setAttribute("src", 'https://report-id.online/api_web/file-upload/test/' + return_data.dataClass.pictureName);
            // document.getElementById('old_image_view').setAttribute("src", 'https://report-id.online/api_web/file-upload/e-learning/image-class/' + return_data.dataClass.pictureName);
            document.getElementById('old_image').setAttribute("value", return_data.dataClass.pictureName);
            document.getElementById('ubah_tujuan_pelatihan').innerHTML = return_data.dataClass.trainingObjectives;

        }
    }
    // // Send the data to PHP now... and wait for response to update the status div
    xhttp.send('id=' + dataId); // Actually execute the request
    // // document.getElementById("status").innerHTML = "processing...";

}

for (let i = 0; i < btnUbah.length; i++) {

    const element = btnUbah[i];

    element.addEventListener('click', () => {
    
        const idClass = element.getAttribute('data-id');
        const dataUrl = element.getAttribute('data-url');

        params = {
            "id"    : idClass,
            "url"   : dataUrl
        };
    
        getDataClassById(params);
    
    });

}

// cek final code
const cekKodeFinal = document.getElementById('cek-kode');
const kodeKopetensi = document.getElementById('kode_kopetensi');
const kodeUnitBisnis = document.getElementById('kode_unit_bisnis');
const modulId = document.getElementById('modul_id');
const kodeKelas = document.getElementById('kode_kelas');
const kodeSubKelas = document.getElementById('kode_sub_kelas');



cekKodeFinal.addEventListener('click', () => {
    const finalCode = kodeKopetensi.value + kodeUnitBisnis.value + "-" + modulId.value + "." + kodeKelas.value + "-" + kodeSubKelas.value;
    const textFinalCode = document.getElementById('text-final-code');
    const statusFinalCode = document.getElementById('status-final-code');
    textFinalCode.parentElement.classList.add("d-none");

    // data
    const dataUrl = cekKodeFinal.getAttribute('data-url');

    const xhttp = new XMLHttpRequest();

    xhttp.open("POST", dataUrl, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // Access the onreadystatechange event for the XMLHttpRequest object
    xhttp.onreadystatechange = function() {

        if(xhttp.readyState == 4 && xhttp.status == 200) {
 
            const return_data = JSON.parse(xhttp.response);


            if (return_data.return == true) {
                textFinalCode.parentElement.classList.remove("d-none");
                textFinalCode.innerHTML = `Kode final <span class="fw-bolder">`+ finalCode +`</span>`;
                statusFinalCode.innerHTML = `<span class="badge bg-danger">Sudah ada</span>`;
            }
            
            if (return_data.return == false) {
                textFinalCode.parentElement.classList.remove("d-none");
                textFinalCode.innerHTML = `Kode final <span class="fw-bolder">`+ finalCode +`</span>`;
                statusFinalCode.innerHTML = `<span class="badge bg-success">Tersedia</span>`;
            }

        }
    }
    // // Send the data to PHP now... and wait for response to update the status div
    xhttp.send('finalCode=' + finalCode); // Actually execute the request
    // // document.getElementById("status").innerHTML = "processing...";
})

