const menuButton = document.getElementById('menuBtn');

function showHideSidebar() {
    var sidebar = document.getElementById("sidebar");
    var content = document.getElementById("content");
    if (sidebar.classList.contains("w-25")) {
        sidebar.classList.remove("w-25");
        sidebar.classList.add("active");
    } else {
        sidebar.classList.remove("active");
        sidebar.classList.add("w-25");
    }

    if (content.classList.contains("w-75")) {
        content.classList.remove("w-75");
        content.classList.add("w-100");
    } else {
        content.classList.add("w-75");
        content.classList.remove("w-100");
    }
    // sidebar.classList.toggle("active");
  }

menuButton.addEventListener('click', function () {
    showHideSidebar();
});

var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
});