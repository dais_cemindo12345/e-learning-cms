const ekstensi = document.querySelector('#ekstensi small');
const radios = document.querySelectorAll('.form-check-input');
// console.log(radio);

function changeTextExtension(text) {
    if (text == 'gambar') {
        ekstensi.innerHTML = "Tipe file : jpg, jpeg, png dan maksimal file 2 MB";
    } else if (text == 'video') {
        ekstensi.innerHTML = "Tipe file : mp4";
    } else {
        ekstensi.innerHTML = "Tipe file : pdf dan maksimal file 2 MB";
    }
}

// var radios = document.forms["formA"].elements["myradio"];
for(var i = 0, max = radios.length; i < max; i++) {
    radios[i].onclick = function() {
        changeTextExtension(this.value)
    }
}

// radio.addEventListener('click', function () {
//     console.log("OK");
//     // console.log(this.textContent);
//     // changeTextExtension(text);
// });