// change type
const tipeSoal = document.getElementsByClassName('tipe-soal');

for (let i = 0; i < tipeSoal.length; i++) {

    const e = tipeSoal[i];

    e.addEventListener('change', function () {

        const valU = e.value;
        const parent = e.parentNode.parentNode;
        const getMcClass = parent.querySelector('.answer .mc'); 
        const getPClass = parent.querySelector('.answer .p');

        
        if (valU == 'p') {
            // p
            getPClass.classList.remove('d-none');
            getPClass.classList.add('d-block');
            
            // mc
            getMcClass.classList.remove('d-block');
            getMcClass.classList.add('d-none');
            
        } 
        
        if (valU == 'mc') {
            // p
            getPClass.classList.remove('d-block');
            getPClass.classList.add('d-none');
            
            // mc
            getMcClass.classList.remove('d-none');
            getMcClass.classList.add('d-bclok');
        } 

    });

}

// add option
const addOptionBtn = document.querySelectorAll('.add-option');

for (let i = 0; i < addOptionBtn.length; i++) {
    
    const e = addOptionBtn[i];
    
    e.addEventListener('click', function () {
        const addOption = e.previousSibling.previousSibling;
        
        const createDiv =  document.createElement('div');
        const newDiv = addOption.appendChild(createDiv);
        newDiv.setAttribute('class', 'd-flex flex-row align-items-center mb-3');

        // create checkbox
        const createInputCB =  document.createElement('input');
        const newInputCB = newDiv.appendChild(createInputCB);
        newInputCB.setAttribute('class', 'form-check-input me-3');
        newInputCB.setAttribute('type', 'checkbox');
        newInputCB.setAttribute('name', 'flexCheckChecked[]');
        
        // create input
        const createInput =  document.createElement('input');
        const newInput = newDiv.appendChild(createInput);
        newInput.setAttribute('class', 'mc-form form-control border-0 border-bottom border-1 me-3');
        newInput.setAttribute('type', 'text');
        newInput.setAttribute('name', 'mc[]');
        newInput.setAttribute('id', 'mc');
        newInput.setAttribute('placeholder', 'Opsi');
        
        // create remove btn
        const createBtn =  document.createElement('button');
        const newBtn = newDiv.appendChild(createBtn);
        newBtn.setAttribute('class', 'remove-option btn-close');
        newBtn.setAttribute('type', 'button');
        
        // remove option
        const removeOption = document.querySelectorAll(".remove-option");

        for (let j = 0; j < removeOption.length; j++) {
            const e2 = removeOption[j];

            e2.addEventListener("click", function () {
                e2.parentElement.remove();
            })
            
        }
    });
    
}


// send data form to controller add_soal
const submitBtn = document.getElementById("submit");
const formSoal = document.getElementById('form-soal');
const questionList = document.getElementById('question-list');

submitBtn.addEventListener("click", function () {
    // get id class
    const idClass = formSoal.querySelector('#id-class').value;
    const urlClass = formSoal.querySelector('#id-class').getAttribute('data-url');

    // get question and question type
    const question = formSoal.querySelector('#pertanyaan').value;
    const questionType = formSoal.querySelector('#tipe-soal').value;
    
    // get point
    const point = formSoal.querySelector('#point').value;
    const point_p = formSoal.querySelector('#point_p').value;

    // check question type for answer
    if (questionType == "mc") {

        // input
        var getAnswer = formSoal.querySelectorAll(".mc-form");
        var arrAnswer = [];
        for (let i = 0; i < getAnswer.length; i++) {
            
            const answerValue = getAnswer[i].value;

            arrAnswer.push(answerValue);
        }
        
        // checkbox
        var getAnswerKey = formSoal.querySelectorAll(".form-check-input");
        var arrAnswerKey = [];
        for (let i = 0; i < getAnswerKey.length; i++) {
            
            const checkbox = getAnswerKey[i];
            var checkedCheckbox = checkbox.checked;

            if (checkedCheckbox == false) {
                var checked = 0;
            }
            
            if (checkedCheckbox == true) {
                var checked = 1;
            }

            arrAnswerKey.push(checked);
        }

        answerKey = arrAnswerKey;
        answer = arrAnswer;

        var dataAnswer = {
            "id"            : idClass,
            "question"      : question,
            "questionType"  : questionType,
            "point"         : point,
            "answerKey"     : answerKey,
            "answer"        : answer
        }
    }
    
    if (questionType == "p") {
        var dataAnswer = {
            "id"            : idClass,
            "question"      : question,
            "questionType"  : questionType,
            "point"         : point_p
        }
    } 

    const xhttp = new XMLHttpRequest();
    xhttp.open("POST", urlClass, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(JSON.stringify(dataAnswer));

    xhttp.onreadystatechange = function() {

        if(xhttp.readyState == 4 && xhttp.status == 200) {
            const return_data = JSON.parse(xhttp.response);

            if (return_data.status == 0 || return_data.status == false) {
                alert(return_data.message);
                location.reload();
            }

            if (return_data.status == 1 || return_data.status == true) {
                alert(return_data.message);
                location.reload();
            }
        }
    }

})



