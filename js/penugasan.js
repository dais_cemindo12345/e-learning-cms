const tugaskan = document.getElementsByClassName("assign");
const assignTo = document.getElementById("assign-to");

for (let i = 0; i < tugaskan.length; i++) {
    const e = tugaskan[i];

    e.addEventListener("click", function () {
        
        const alertAssignment = document.getElementById("alertAssignment");

        alertAssignment.innerHTML = "";

        const dataId = this.getAttribute('data-id');
        const dataNik = this.getAttribute('data-nik');
        const dataNama = this.getAttribute('data-nama');
        const dataKelas = this.getAttribute('data-kelas');
        const dataUrl = this.getAttribute('data-url');

        // Send data
        const params = {
            'id'        : dataId,
            'nik'       : dataNik,
            'nama'      : dataNama,
            'kelas'     : dataKelas,
        } 

        const url = dataUrl + 'kelas/assign_employees';

        const xhttp = new XMLHttpRequest();
        xhttp.open("POST", url, true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(JSON.stringify(params));

        xhttp.onreadystatechange = function() {

            if(xhttp.readyState == 4 && xhttp.status == 200) {
                const return_data = JSON.parse(xhttp.response);
                // const return_data = xhttp.response;
                // console.log(return_data);

                if (return_data.status == 0) {
                    alertAssignment.innerHTML = `
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        `+ return_data.message +`.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    ` ;
                }

                if (return_data.status == 1) {

                    const assignNull = document.getElementById("assign-null");
                    if (assignNull != null ) {
                        assignNull.classList.add('d-none');
                    }

                    alertAssignment.innerHTML = `
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        `+ return_data.message +`.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    ` ;

                    var newData = `
                    <div class="bg-mc5 p-2 d-inline-block rounded-2 align-items-center me-2">
                        <span class="text-dark">`+ dataNama +`</span>
                        <a href="#" class="cancle-assignment"><img src="<?= base_url() ?>assets/web/icon/close_danger.svg" alt="delete"></a>
                    </div>`;

                    // div
                    const createDiv = document.createElement('div'); 
                    const newDiv = assignTo.appendChild(createDiv);
                    newDiv.setAttribute('class', 'bg-mc5 p-2 d-inline-block rounded-2 align-items-center me-2 mb-2');
                    
                    // span
                    const createSpan = document.createElement('span'); 
                    const newSpan = newDiv.appendChild(createSpan);
                    newSpan.setAttribute('class', 'text-dark');
                    
                    // text span
                    const createTextSpan = document.createTextNode(dataNama); 
                    newSpan.appendChild(createTextSpan);
                    
                    // a
                    const createA = document.createElement('a'); 
                    const newA = newDiv.appendChild(createA);
                    newA.setAttribute('class', 'cancle-assignment');
                    newA.setAttribute('href', '#');
                    newA.setAttribute('data-url', dataUrl);
                    newA.setAttribute('data-nik', dataNik);
                    newA.setAttribute('data-kelas', dataKelas);
                    
                    // img a
                    const createImg = document.createElement('img'); 
                    const newImg = createA.appendChild(createImg);
                    newImg.setAttribute('src', dataUrl + '/assets/web/icon/close_danger.svg');
                    newImg.setAttribute('alt', 'delete');

                    assignTo.appendChild(newDiv);
                }
            }
        }

    })
}

const cancleAssign = document.getElementsByClassName('cancle-assignment');
// console.log(cancleAssign);

for (let i = 0; i < cancleAssign.length; i++) {
    const e = cancleAssign[i];
    
    e.addEventListener('click', function () {

        // console.log(e);

        const dataNik = this.getAttribute('data-nik');
        const dataKelas = this.getAttribute('data-kelas');
        const dataUrl = this.getAttribute('data-url');

        // Send data
        const params = {
            'nik'       : dataNik,
            'kelas'     : dataKelas,
        } 

        const url = dataUrl + 'kelas/delete_assignment';

        const xhttp = new XMLHttpRequest();
        xhttp.open("POST", url, true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(JSON.stringify(params));

        const dataE = this;

        xhttp.onreadystatechange = function() {

            const dataD = dataE;
            
            if(xhttp.readyState == 4 && xhttp.status == 200) {
                const return_data = xhttp.response;
                
                if (return_data.status == 0) {
                    alertAssignment.innerHTML = `
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    `+ return_data.message +`.
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    ` ;                    
                }
                
                if (return_data.status == 1) {
                    if (dataC != null ) {
                        dataC.classList.add('d-none');
                    }
                }
                dataD.parentNode.classList.add('d-none');
            }
        }
    })

}